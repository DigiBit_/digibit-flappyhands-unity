//
//  iosplugin.h
//  iosplugin
//
//  Created by Muhammad Uzair on 9/8/17.
//  Copyright © 2017 DigiBit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface iosplugin : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

+( iosplugin * ) plugin;
-(id) init;
-(void) setSerial: (NSString*) serial;
-(void) setObjectName: (NSString*) ObjectName;
-(bool) sendCommand: (NSString*) Command;
-(NSString *) getDigibitPidData;
-(NSString*) getTimeStampDiffPrimary;
-(NSString*) getTimeStampDiffSecondary;
-(NSString*) getTimeStampDiff;
-(void) enableLogs: (bool) Value;
-(void) onApplicationPaused: (bool) Paused;
@end
