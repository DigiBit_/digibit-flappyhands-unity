﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DigiBitSDK;
using UnityEngine.XR;

public class PluginSceneManager : MonoBehaviour
{

    //edit this script as necessary but keep the IsActive bool there to check for multiple instance of scene
    //Led pattern requires new firmware
    //game should be paused when digibits disconnect as this script does not handle game pause

    static bool IsActive = false; //to prevent multiple instances of scene to load

    public UnityEngine.Video.VideoPlayer CalVideoPlayer;
    public UnityEngine.Video.VideoClip CalVideoIntro;
    public UnityEngine.AudioSource CalAudioSource;

    public GameObject FlashingPanel;
    public GameObject WaitingForConnectionPanel;
    public GameObject WaitingForGameToStartPanel;
    public GameObject FWInfoPanel;
    public GameObject LeftPanel;
    public GameObject RightPanel;

    public GameObject RightHand;
    public GameObject LeftHand;
    public GameObject RightFoot;
    public GameObject LeftFoot;

    public GameObject RightRedLED1;
    public GameObject RightRedLED2;
    public GameObject RightRedLED3;
    public GameObject LeftRedLED1;
    public GameObject LeftRedLED2;
    public GameObject LeftRedLED3;
    public GameObject RightGreenLED1;
    public GameObject RightGreenLED2;
    public GameObject RightGreenLED3;
    public GameObject LeftGreenLED1;
    public GameObject LeftGreenLED2;
    public GameObject LeftGreenLED3;
    public GameObject RightBlueLED1;
    public GameObject RightBlueLED2;
    public GameObject RightBlueLED3;
    public GameObject LeftBlueLED1;
    public GameObject LeftBlueLED2;
    public GameObject LeftBlueLED3;
    public GameObject RightWhiteLED1;
    public GameObject RightWhiteLED2;
    public GameObject RightWhiteLED3;
    public GameObject LeftWhiteLED1;
    public GameObject LeftWhiteLED2;
    public GameObject LeftWhiteLED3;

    public Text GameResumeCounter;
    public Text FWVersionInfoText;
    public Text ConfirmText;
    public Text TextUL;
    public Text TextUR;
    public Text TextLL;
    public Text TextLR;

    public bool PlayWithoutDigiBits = false;
    private bool LEDDefaultPattern = true;

    bool WaitForClapActive = false;

    enum SelectedButton
    {
        UpperLeft,
        UpperRight,
        LowerLeft,
        LowerRight
    };

    SelectedButton SwipeButton = SelectedButton.UpperLeft;

    enum CalibrationStage
    {
        WaitingForVideo,
        Done,
    };

    CalibrationStage PlayerStage = CalibrationStage.WaitingForVideo;

    Scene CurrentScene;
    int RequiredFirmwareVersion = 4;

    void Awake()
    {
        if (IsActive)
        {
            SceneManager.UnloadSceneAsync("PluginStartUp");
            return;
        }
        IsActive = true;
        DigiBit.InhibitInput = true;
    }

    void Start()
    {
        if (XRSettings.loadedDeviceName == "cardboard")
        {
            ContinueGame();
            return;
        }

        FlashingPanel.SetActive(false);
        WaitingForConnectionPanel.SetActive(true);

        CurrentScene = SceneManager.GetActiveScene();
        ManageCameraPosition();
        ManageEventSystem();
        ManageSortingOrder();

        if (PlayerStage != CalibrationStage.Done)
        {
            StartCoroutine(FirmwareInfo());
        }

        DefaultLEDs();

        if (DigiBit.OnFeet)
        {
            RightHand.SetActive(false);
            LeftHand.SetActive(false);
            RightFoot.SetActive(true);
            LeftFoot.SetActive(true);
        }
        else
        {
            RightHand.SetActive(true);
            LeftHand.SetActive(true);
            RightFoot.SetActive(false);
            LeftFoot.SetActive(false);
        }
    }

    void Update()
    {
        if (PlayWithoutDigiBits)
        {
            DigiBit.PlayWithoutDigiBits = true;
            GoToGame();
        }

        //#if !(UNITY_IOS || UNITY_ANDROID)
        // We do not have calibration working on the PC.
        //GoToGame ();
        //#endif

        if (PlayerStage == CalibrationStage.Done)
            return;

        if (FlashingPanel.activeInHierarchy)
        {
            if (SwipeButton == SelectedButton.UpperLeft && DigiBit.SwipeRight)
            {
                SwipeButton = SelectedButton.UpperRight;
                TextUL.text = "Top Left";
                TextUL.color = new Color32(50, 50, 50, 255);
                TextUR.text = "> Top Right <";
                TextUR.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.UpperLeft && DigiBit.SwipeDown)
            {
                SwipeButton = SelectedButton.LowerLeft;
                TextUL.text = "Top Left";
                TextUL.color = new Color32(50, 50, 50, 255);
                TextLL.text = "> Bottom Left <";
                TextLL.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.LowerLeft && DigiBit.SwipeUp)
            {
                SwipeButton = SelectedButton.UpperLeft;
                TextLL.text = "Bottom Left";
                TextLL.color = new Color32(50, 50, 50, 255);
                TextUL.text = "> Top Left <";
                TextUL.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.LowerLeft && DigiBit.SwipeRight)
            {
                SwipeButton = SelectedButton.LowerRight;
                TextLL.text = "Bottom Left";
                TextLL.color = new Color32(50, 50, 50, 255);
                TextLR.text = "> Bottom Right <";
                TextLR.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.UpperRight && DigiBit.SwipeLeft)
            {
                SwipeButton = SelectedButton.UpperLeft;
                TextUR.text = "Top Right";
                TextUR.color = new Color32(50, 50, 50, 255);
                TextUL.text = "> Top Left <";
                TextUL.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.UpperRight && DigiBit.SwipeDown)
            {
                SwipeButton = SelectedButton.LowerRight;
                TextUR.text = "Top Right";
                TextUR.color = new Color32(50, 50, 50, 255);
                TextLR.text = "> Bottom Right <";
                TextLR.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.LowerRight && DigiBit.SwipeLeft)
            {
                SwipeButton = SelectedButton.LowerLeft;
                TextLR.text = "Bottom Right";
                TextLR.color = new Color32(50, 50, 50, 255);
                TextLL.text = "> Bottom Left <";
                TextLL.color = new Color32(255, 255, 255, 255);
            }
            else if (SwipeButton == SelectedButton.LowerRight && DigiBit.SwipeUp)
            {
                SwipeButton = SelectedButton.UpperRight;
                TextLR.text = "Bottom Right";
                TextLR.color = new Color32(50, 50, 50, 255);
                TextUR.text = "> Upper Right <";
                TextUR.color = new Color32(255, 255, 255, 255);
            }
        }

        // If we passed calibration alrady do not do it a second time.
        if (!DigiBit.PrimaryConnected || !DigiBit.SecondaryConnected)
        {
            WaitingForConnectionPanel.SetActive(true);
            FlashingPanel.SetActive(false);
        }
        else if (DigiBit.PrimaryData.FirmwareVersion >= 0 && DigiBit.SecondaryData.FirmwareVersion >= 0 && PlayerStage == CalibrationStage.WaitingForVideo)
        {
            PlayerStage = CalibrationStage.WaitingForVideo;
            WaitingForConnectionPanel.SetActive(false);
            FlashingPanel.SetActive(true);
            if (!WaitForClapActive)
                StartCoroutine(WaitForClap());
        }
    }

    IEnumerator WaitForClap()
    {
        WaitForClapActive = true;

        while (true)
        {
            if (DigiBit.PrimaryData.Tap && DigiBit.SecondaryData.Tap)
            {
                while (DigiBit.PrimaryData.Tap && DigiBit.SecondaryData.Tap)
                    yield return new WaitForSeconds(0.01f);
                ContinueGame();
                break;
            }
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator FirmwareInfo()
    {
        while (true)
        {
            if (DigiBit.PrimaryData.FirmwareVersion == -1 || DigiBit.SecondaryData.FirmwareVersion == -1)
            {
                FWVersionInfoText.GetComponent<Text>().text = "Firmware version not found.";
                yield return new WaitForSecondsRealtime(0.15f);
            }
            else if ((DigiBit.PrimaryData.FirmwareVersion < RequiredFirmwareVersion &&
                     DigiBit.SecondaryData.FirmwareVersion <= RequiredFirmwareVersion))
            {

                FWVersionInfoText.GetComponent<Text>().text = "Old firmware: " + RequiredFirmwareVersion + " Version found: " +
                DigiBit.PrimaryData.FirmwareVersion + " and " + DigiBit.SecondaryData.FirmwareVersion + "\n Use DigiBit Connect to update.";

                FWInfoPanel.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                FWInfoPanel.SetActive(true);
                yield return new WaitForSeconds(0.1f);
                FWInfoPanel.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                FWInfoPanel.SetActive(true);
                yield return new WaitForSeconds(1.5f);
                FWInfoPanel.SetActive(false);
            }
            else
            {
                FWVersionInfoText.GetComponent<Text>().text = "";
            }

            yield return new WaitForSecondsRealtime(0.1f);
        }
    }

    void ManageCameraPosition()
    {
        float MaxZposition = 0.0f;
        if (CurrentScene.name != "PluginStartUp")
        {
            foreach (GameObject GameObj in CurrentScene.GetRootGameObjects())
            {

                foreach (Transform T in GameObj.GetComponentsInChildren<Transform>(true))
                {
                    if (MaxZposition < T.position.z)
                        MaxZposition = T.position.z;
                }
            }
        }

        Transform CameraTransform = gameObject.GetComponentInChildren<Camera>().gameObject.transform;
        CameraTransform.position = new Vector3(CameraTransform.position.x, CameraTransform.position.y, MaxZposition + 1.0f);
    }

    void ManageEventSystem()
    {
        if (GameObject.FindObjectOfType<EventSystem>() == null)
            gameObject.GetComponentInChildren<EventSystem>(true).gameObject.SetActive(true);
    }

    void ManageSortingOrder()
    {
        int SortOrder = 1000; //need better way to find this in scene
        string SortingLayerName = SortingLayer.layers[SortingLayer.layers.Length - 1].name;
        FlashingPanel.GetComponent<Canvas>().sortingLayerName = SortingLayerName;
        FlashingPanel.GetComponent<Canvas>().sortingOrder = SortOrder;
        WaitingForConnectionPanel.GetComponent<Canvas>().sortingLayerName = SortingLayerName;
        WaitingForConnectionPanel.GetComponent<Canvas>().sortingOrder = SortOrder;
    }

    #region ColorButtonCallbacks
    public void SetLEDs(Devices deviceSelection, LED led1, LED led2, LED led3)
    {
        if (deviceSelection == Devices.BOTH || deviceSelection == Devices.PRIMARY)
        {
            LeftRedLED1.SetActive(led1.Red != 0);
            LeftWhiteLED1.SetActive(led1.White != 0);
            LeftBlueLED1.SetActive(led1.Blue != 0);
            LeftGreenLED1.SetActive(led1.Green != 0);

            LeftRedLED2.SetActive(led2.Red != 0);
            LeftWhiteLED2.SetActive(led2.White != 0);
            LeftBlueLED2.SetActive(led2.Blue != 0);
            LeftGreenLED2.SetActive(led2.Green != 0);

            LeftRedLED3.SetActive(led3.Red != 0);
            LeftWhiteLED3.SetActive(led3.White != 0);
            LeftBlueLED3.SetActive(led3.Blue != 0);
            LeftGreenLED3.SetActive(led3.Green != 0);
        }

        if (deviceSelection == Devices.BOTH || deviceSelection == Devices.SECONDARY)
        {
            RightRedLED1.SetActive(led1.Red != 0);
            RightWhiteLED1.SetActive(led1.White != 0);
            RightBlueLED1.SetActive(led1.Blue != 0);
            RightGreenLED1.SetActive(led1.Green != 0);

            RightRedLED2.SetActive(led2.Red != 0);
            RightWhiteLED2.SetActive(led2.White != 0);
            RightBlueLED2.SetActive(led2.Blue != 0);
            RightGreenLED2.SetActive(led2.Green != 0);

            RightRedLED3.SetActive(led3.Red != 0);
            RightWhiteLED3.SetActive(led3.White != 0);
            RightBlueLED3.SetActive(led3.Blue != 0);
            RightGreenLED3.SetActive(led3.Green != 0);
        }

        DigiBit.SetLEDs(deviceSelection, led3, led2, led1);
    }

    public void AllLEDsOff()
    {
        LEDDefaultPattern = false;
        SetLEDs(Devices.BOTH, new LED(0, 0, 0, 0), new LED(0, 0, 0, 0), new LED(0, 0, 0, 0));
    }

    public void BlueOn()
    {
        AllLEDsOff();
        SetLEDs(Devices.BOTH, new LED(0, 0, 255, 0), new LED(0, 0, 255, 0), new LED(0, 0, 255, 0));
    }

    public void DefaultLEDs()
    {
        AllLEDsOff();

        LEDDefaultPattern = true;
        StartCoroutine(FlashDefaultLEDs());
    }

    IEnumerator FlashDefaultLEDs()
    {
        LED ONGREEN = new LED(0, 255, 0, 0);
        LED ONBLUE = new LED(0, 0, 255, 0);
        LED OFF = new LED(0, 0, 0, 0);

        while (LEDDefaultPattern)
        {

            if (DigiBit.PrimaryConnected && DigiBit.SecondaryConnected)
            {
                for (int i = 0; i < 2; i++)
                {
                    SetLEDs(Devices.PRIMARY, ONGREEN, OFF, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.PRIMARY, ONGREEN, ONGREEN, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.PRIMARY, ONGREEN, ONGREEN, ONGREEN);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.BOTH, OFF, OFF, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                }

                for (int i = 0; i < 2; i++)
                {
                    SetLEDs(Devices.SECONDARY, ONBLUE, OFF, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.SECONDARY, ONBLUE, ONBLUE, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.SECONDARY, ONBLUE, ONBLUE, ONBLUE);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                    SetLEDs(Devices.BOTH, OFF, OFF, OFF);
                    yield return new WaitForSecondsRealtime(0.25f);
                    if (!LEDDefaultPattern) break;
                }
            }
            else
                yield return new WaitForSecondsRealtime(0.25f);
        }
    }
    #endregion

    public void GoToGame()
    {
        SceneManager.UnloadSceneAsync("PluginStartUp");
        IsActive = false;
        DigiBit.InhibitInput = false;
        PlayerStage = CalibrationStage.Done;
    }

    IEnumerator PlayCalVideo(UnityEngine.Video.VideoClip videoClip)
    {
        CalVideoPlayer.Stop();

        //Disable Play on Awake for both Video and Audio
        CalVideoPlayer.playOnAwake = false;

        //We want to play from video clip not from url
        CalVideoPlayer.source = UnityEngine.Video.VideoSource.VideoClip;

        //Loop forever
        CalVideoPlayer.isLooping = true;

        //Set Audio Output to AudioSource
        CalVideoPlayer.audioOutputMode = UnityEngine.Video.VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        CalVideoPlayer.EnableAudioTrack(0, true);
        CalVideoPlayer.SetTargetAudioSource(0, CalAudioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        CalVideoPlayer.clip = videoClip;
        CalVideoPlayer.Prepare();

        //Wait until video is prepared
        while (!CalVideoPlayer.isPrepared)
        {
            Debug.Log("Preparing Video");
            yield return null;
        }

        Debug.Log("Done Preparing Video");

        //Play Video
        CalVideoPlayer.Play();

        Debug.Log("Playing Video");
    }

    public void ContinueGame()
    {
        PlayerStage = CalibrationStage.Done;
        GoToGame();
    }
}