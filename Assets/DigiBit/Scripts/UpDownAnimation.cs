﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownAnimation : MonoBehaviour {
	private Vector3  posStart;
	private Vector3  posEnd;
	private Vector3 posNow;
	private float animationSpeed = 1.0f;
	private float animationDistance = 0.5f;
	private bool bUp = true;

	// Use this for initialization
	void Start () {
		posStart = transform.position;
		posEnd = posStart; 
		posEnd.y += animationDistance;
	}
	
	// Update is called once per frame
	void Update () {
		posNow = transform.position;
		if (bUp) {
			transform.position += new Vector3 (0.0f, animationSpeed * 0.02f, 0.0f); 
			if (transform.position.y - posEnd.y > 0.0f)
				bUp = false;
		} else {
			transform.position -= new Vector3 (0.0f, animationSpeed * 0.02f, 0.0f); 
			if (transform.position.y - posStart.y < 0.0f)
				bUp = true;			
		}
	}
}

