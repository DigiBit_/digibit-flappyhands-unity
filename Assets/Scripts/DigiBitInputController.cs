﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigiBitSDK;

public class DigiBitInputController : MonoBehaviour {

    public static DigiBitInputController instance;
	void Start () {
        instance = this;
		DigiBitCallBacks.OnDeviceConnected += OnDeviceConnected;
		/// <summary>
		/// Init the specified SerialNumber, EnableLogs and EnablePredefinedPid.
		/// </summary>
		/// <param name="SerialNumber">Serial number.</param>
		/// <param name="EnableLogs">If set to <c>true</c> enable logs.</param>
		/// <param name="DigiBitsWornOnFeet">If set to <c>true</c> feet logic = true, hands logic = false.</param>
		/// <param name="SelectedPredefinedPid">Select PrederinedPid options from enum.</param>
		/// <param name="EnableYawCalibrationIn">If set to <c>true</c> position or yaw calibration is enabled.</param>
		DigiBit.Init (true, false, PredefinedPid.PidList2);
	}

	void OnDeviceConnected (Devices device)
	{
		
	}

    public void SetLeds()
    {

        StartCoroutine(TurnOnLEDs(Devices.PRIMARY));
        StartCoroutine(TurnOnLEDs(Devices.SECONDARY));
    }

	IEnumerator TurnOnLEDs(Devices device)
	{
		yield return new WaitForSeconds(0.5f);

		if (device == Devices.PRIMARY)
			DigiBit.SetLEDs (Devices.PRIMARY, new LED (0, 255, 0, 0), new LED (0, 255, 0, 0), new LED (0, 255, 0, 0));
		if (device == Devices.SECONDARY)
			DigiBit.SetLEDs (Devices.SECONDARY, new LED (0, 0, 255, 0), new LED (0, 0, 255, 0), new LED (0, 0, 255, 0));
	}		
}
