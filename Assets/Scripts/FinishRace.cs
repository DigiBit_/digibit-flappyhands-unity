﻿using UnityEngine;
using System.Collections;

public class FinishRace : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider){

		if(collider.GetComponent<Bird>() != null){

			if (!collider.GetComponent<Bird> ().isDead) {
				collider.GetComponent<Bird> ().RaceWon();
			}
		}
	}

	void Start(){
		gameObject.GetComponent<BoxCollider2D> ().size = new Vector2 (Screen.width*0.05f, Screen.height);
	}
}

