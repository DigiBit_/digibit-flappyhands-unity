﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigiBitSDK;

public class MenuScript : MonoBehaviour {

	public GameObject Easy;
	public GameObject Medium;
	public GameObject Hard;
	public GameObject StartRace;
	public GameObject DigiBitsOnHand;
	public GameObject DigiBitsOnHand2Player;
	public Toggle Toggle2PlayerMode;
	public Toggle Toggle2PlayerRaceMode;
	public Toggle ToggleSinglePlayerMode;

	bool PrimaryMoveRight;
	bool SecondaryMoveRight;
	bool PrimaryMoveRightLast;
	bool SecondaryMoveRightLast;
	bool PrimaryMoveLeft;
	bool SecondaryMoveLeft;
	bool PrimaryMoveLeftLast;
	bool SecondaryMoveLeftLast;
	bool PrimaryMoveUp;
	bool SecondaryMoveUp;
	bool PrimaryMoveUpLast;
	bool SecondaryMoveUpLast;
	bool PrimaryMoveDown;
	bool SecondaryMoveDown;
	bool PrimaryMoveDownLast;
	bool SecondaryMoveDownLast;

	public GameObject HowToPlayPanel;

	void Start(){
		Debug.Log("DIGIBIT FLAPPYHANDS Wait For Clap");
	}

	void Update () {
		
		ClearButtonHighlight ();
		ManageSelectedButtonHighlight ();

		if (DigiBit.PrimaryData.Tap || DigiBit.SecondaryData.Tap || Input.GetKey(KeyCode.A)) {
			GameControl.instance.LevelSelected = true;
		}

		if ((!DigiBit.PrimaryConnected || !DigiBit.SecondaryConnected) && !GameControl.instance.PlayWithoutDigibits) {
			GameControl.instance.StartDelayStarted = false;
			gameObject.SetActive (false);
	    } 
	}

	void MoveLogic(){
		MoveLeftRightLogic ();
		MoveUpDownLogic ();

	}

	void MoveLeftRightLogic(){

		PrimaryMoveLeft = (PrimaryMoveLeftLast == DigiBit.PrimaryData.MoveLeft) ? false : DigiBit.PrimaryData.MoveLeft;
		PrimaryMoveLeftLast = DigiBit.PrimaryData.MoveLeft;

		PrimaryMoveRight = (PrimaryMoveRightLast == DigiBit.PrimaryData.MoveRight) ? false : DigiBit.PrimaryData.MoveRight; 
		PrimaryMoveRightLast = DigiBit.PrimaryData.MoveRight;

		SecondaryMoveLeft = (SecondaryMoveLeftLast == DigiBit.SecondaryData.MoveLeft) ? false : DigiBit.SecondaryData.MoveLeft; 
		SecondaryMoveLeftLast = DigiBit.SecondaryData.MoveLeft;

		SecondaryMoveRight = (SecondaryMoveRightLast == DigiBit.SecondaryData.MoveRight) ? false : DigiBit.SecondaryData.MoveRight; 
		SecondaryMoveRightLast = DigiBit.SecondaryData.MoveRight;
	}

	void MoveUpDownLogic(){

		PrimaryMoveUp = (PrimaryMoveUpLast == DigiBit.PrimaryData.MoveUp) ? false : DigiBit.PrimaryData.MoveUp;
		PrimaryMoveUpLast = DigiBit.PrimaryData.MoveUp;

		PrimaryMoveDown = (PrimaryMoveDownLast == DigiBit.PrimaryData.MoveDown) ? false : DigiBit.PrimaryData.MoveDown; 
		PrimaryMoveDownLast = DigiBit.PrimaryData.MoveDown;

		SecondaryMoveUp = (SecondaryMoveUpLast == DigiBit.SecondaryData.MoveUp) ? false : DigiBit.SecondaryData.MoveUp; 
		SecondaryMoveUpLast = DigiBit.SecondaryData.MoveUp;

		SecondaryMoveDown = (SecondaryMoveDownLast == DigiBit.SecondaryData.MoveDown) ? false : DigiBit.SecondaryData.MoveDown; 
		SecondaryMoveDownLast = DigiBit.SecondaryData.MoveDown;
	}

	void ClearButtonHighlight(){
		Easy.GetComponent<Image> ().enabled = false;
		Medium.GetComponent<Image> ().enabled = false;
		Hard.GetComponent<Image> ().enabled = false;
	}

	void ManageSelectedButtonHighlight(){

		// Perform MoveRight / Left / Up / Down Logic
		MoveLogic();
	
		if (ColumnPool.instance.level == ColumnPool.Level.Easy) {
			if (PrimaryMoveRight || SecondaryMoveRight) {
				ColumnPool.instance.level = ColumnPool.Level.Medium;
				Medium.GetComponent<Image> ().enabled = true;
			} else {
				Easy.GetComponent<Image> ().enabled = true;
			}
		} else if (ColumnPool.instance.level == ColumnPool.Level.Medium) {
			if (PrimaryMoveRight || SecondaryMoveRight) {
				ColumnPool.instance.level = ColumnPool.Level.Hard;
				Hard.GetComponent<Image> ().enabled = true;
			} else if (PrimaryMoveLeft || SecondaryMoveLeft) {
				ColumnPool.instance.level = ColumnPool.Level.Easy;
				Easy.GetComponent<Image> ().enabled = true;
			} else {
				Medium.GetComponent<Image> ().enabled = true;
			}
		} else if (ColumnPool.instance.level == ColumnPool.Level.Hard) {
			if (PrimaryMoveLeft || SecondaryMoveLeft) {
				ColumnPool.instance.level = ColumnPool.Level.Medium;
				Medium.GetComponent<Image> ().enabled = true;
			} else {
				Hard.GetComponent<Image> ().enabled = true;
			}
		}

		if (GameControl.instance._1PlayerMode) {
			if (PrimaryMoveDown || SecondaryMoveDown) {
				GameControl.instance._1PlayerMode = false;
				ToggleSinglePlayerMode.isOn = false;
				GameControl.instance._2PlayerMode = true;
				Toggle2PlayerMode.isOn = true;
				GameControl.instance._2PlayerRaceMode = false;
				Toggle2PlayerRaceMode.isOn = false;
			}
		} else if (GameControl.instance._2PlayerMode) {
			if (PrimaryMoveDown || SecondaryMoveDown) {
				GameControl.instance._1PlayerMode = false;
				ToggleSinglePlayerMode.isOn = false;
				GameControl.instance._2PlayerMode = false;
				Toggle2PlayerMode.isOn = false;
				GameControl.instance._2PlayerRaceMode = true;
				Toggle2PlayerRaceMode.isOn = true;
			} else if (PrimaryMoveUp || SecondaryMoveUp) {
				GameControl.instance._1PlayerMode = true;
				ToggleSinglePlayerMode.isOn = true;
				GameControl.instance._2PlayerMode = false;
				Toggle2PlayerMode.isOn = false;
				GameControl.instance._2PlayerRaceMode = false;
				Toggle2PlayerRaceMode.isOn = false;
			}
		} else if (GameControl.instance._2PlayerRaceMode) {
			if (PrimaryMoveUp || SecondaryMoveUp) {
				GameControl.instance._1PlayerMode = false;
				ToggleSinglePlayerMode.isOn = false;
				GameControl.instance._2PlayerMode = true;
				Toggle2PlayerMode.isOn = true;
				GameControl.instance._2PlayerRaceMode = false;
				Toggle2PlayerRaceMode.isOn = false;
			}			
		}			
	}

	public void DisableHowToPlayPanel(){
		HowToPlayPanel.SetActive (false);
	}

	public void EnableHowToPlayPanel(){
		HowToPlayPanel.SetActive (true);
	}

	public void OnPlayerModeChanged(bool val){
		GameControl.instance._1PlayerMode = ToggleSinglePlayerMode.isOn;
		GameControl.instance._2PlayerMode = Toggle2PlayerMode.isOn;
		GameControl.instance._2PlayerRaceMode = Toggle2PlayerRaceMode.isOn;


		if (Toggle2PlayerMode.isOn || Toggle2PlayerRaceMode.isOn) {
			DigiBitsOnHand.SetActive (false);
			DigiBitsOnHand2Player.SetActive (true);
		} else {
			DigiBitsOnHand.SetActive (true);
			DigiBitsOnHand2Player.SetActive (false);
		}

		if (Toggle2PlayerRaceMode.isOn) {
			Easy.SetActive (false);
			Medium.SetActive (false);
			Hard.SetActive (false);
			StartRace.SetActive (true);
		} else {
			Easy.SetActive (true);
			Medium.SetActive (true);
			Hard.SetActive (true);
			StartRace.SetActive (false);
		}
	}
}
