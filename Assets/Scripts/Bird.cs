﻿using UnityEngine;
using System.Collections;
using DigiBitSDK;

public class Bird : MonoBehaviour 
{
	public float upForce;	
	public float sideForce;
	public float AirResistanceForce;
	public bool isDead = false;	
	public bool WonRace = false;
	public float MaxYRaceMode;
	public float MinXRaceMode;
	public float MaxXRaceMode;
	private Animator anim;					
	private Rigidbody2D rb2d;

	public bool EnableDynamics = true;
	//Flapping Logic
	public float MinFlappingAngularVelocityDown;
	public float MinFlappingAngularVelocityUp;
	bool LeftAngleUp;
	bool RightAngleUp;
	float AngleVelLeft;
	float AngleVelRight;
	bool IsFlapping;
	bool LeftHandFlapped;
	bool RightHandFlapped;

	//Scene REset Variables
	Vector3 StartPosition;
	Quaternion StartRotation;
	float GravityScale;

	//2 player control
	public bool PrimaryBird;
	public KeyCode Flap = KeyCode.UpArrow;
	public Devices ControllingDevice = Devices.PRIMARY;


	//Independent Score
	public int Score = 0;

	void Start()
	{
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D>();

		GravityScale = rb2d.gravityScale;
		StartPosition = transform.position;
		StartRotation = transform.rotation;
	}

	void Update()
	{
		//max y and min x limiter
		if (GameControl.instance._2PlayerRaceMode) {
			if (isDead == false && !GameControl.instance.GamePaused && EnableDynamics)
				rb2d.AddForce(new Vector2(-AirResistanceForce, 0));
			if (transform.position.y > MaxYRaceMode)
				transform.position = new Vector3 (transform.position.x, MaxYRaceMode, transform.position.z);
			if (transform.position.x < MinXRaceMode)
				transform.position = new Vector3 (MinXRaceMode, transform.position.y, transform.position.z);
		}
		if (isDead == false && !GameControl.instance.GamePaused && EnableDynamics) 
		{
			CheckForFlapping ();
			if (IsFlapping || (GameControl.instance.PlayWithoutDigibits && Input.GetKeyDown(Flap))) 
			{
				AudioManager.instance.PlayFlappingSound();
				anim.SetTrigger("Flap");
				rb2d.velocity = Vector2.zero;
				rb2d.AddForce(new Vector2(0, upForce));
				if (GameControl.instance._2PlayerRaceMode)
					rb2d.AddForce(new Vector2(sideForce,0));
			}

		}
		if (GameControl.instance.GamePaused || !EnableDynamics) {
			rb2d.gravityScale = 0;
			rb2d.velocity = Vector2.zero;
		} else {
			rb2d.gravityScale = GravityScale;
		}
			
	}
		
	public void RaceWon(){
		WonRace = true;
		GameControl.instance.RaceFinished ();		
	}

	public void ResetBird(){
		isDead = false;
		transform.position = StartPosition;
		transform.rotation = StartRotation;
		rb2d.velocity = Vector2.zero;
		rb2d.angularVelocity = 0;
		anim.SetTrigger("Reset");
		LeftAngleUp = true;
		RightAngleUp = true;
		Score = 0;
		WonRace = false;
		EnableDynamics = true;
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (isDead)
			return;
		
		if (!GameControl.instance.GameOver)
			AudioManager.instance.PlayHitSound ();
		rb2d.velocity = Vector2.zero;
		isDead = true;
		anim.SetTrigger ("Die");
		GameControl.instance.BirdDied ();
	}

	public void ForceDeath(){
		rb2d.velocity = Vector2.zero;
		isDead = true;
		anim.SetTrigger ("Die");
	}

	void CheckForFlapping(){

		//Digibit Inputs
		AngleVelLeft = DigiBit.PrimaryData.AngularVelocity.y;
		AngleVelRight = DigiBit.SecondaryData.AngularVelocity.y;

		// A flap is defined as the DigiBits moving up in angle by X first, then down by X.
		if (AngleVelLeft < -MinFlappingAngularVelocityUp)
			LeftAngleUp = true;
		if (AngleVelRight < -MinFlappingAngularVelocityUp)
			RightAngleUp = true;

		//FlappingLogic
		if (LeftAngleUp && (AngleVelLeft > MinFlappingAngularVelocityDown))
			LeftHandFlapped = true;

		if (RightAngleUp && (AngleVelRight > MinFlappingAngularVelocityDown))
			RightHandFlapped = true;

		if (LeftHandFlapped && RightHandFlapped && !(GameControl.instance._2PlayerMode || GameControl.instance._2PlayerRaceMode)) {
			IsFlapping = true;
			LeftHandFlapped = false;
			RightHandFlapped = false;
			LeftAngleUp = false;
			RightAngleUp = false;
		} else if ((GameControl.instance._2PlayerMode || GameControl.instance._2PlayerRaceMode) && ControllingDevice == Devices.PRIMARY && LeftHandFlapped) {
			IsFlapping = true;
			LeftHandFlapped = false;
			LeftAngleUp = false;
		} else if ((GameControl.instance._2PlayerMode || GameControl.instance._2PlayerRaceMode) && ControllingDevice == Devices.SECONDARY && RightHandFlapped) {
			IsFlapping = true;
			RightHandFlapped = false;
			RightAngleUp = false;
		} else
			IsFlapping = false;
	}
}

