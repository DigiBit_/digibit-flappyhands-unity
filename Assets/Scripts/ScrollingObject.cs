﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour 
{
	private Rigidbody2D rb2d;

	Vector3 StartPosition;
	// Use this for initialization
	void Start () 
	{
		//Get and store a reference to the Rigidbody2D attached to this GameObject.
		rb2d = GetComponent<Rigidbody2D>();


	}

	void Update()
	{
		// If the game is over, stop scrolling.
		if (GameControl.instance.GameOver || GameControl.instance.GamePaused || GameControl.instance.GameEnd) {
			rb2d.velocity = Vector2.zero;
		} else {
			rb2d.velocity = new Vector2 (GameControl.instance.scrollSpeed, 0);
		}
	}
}
