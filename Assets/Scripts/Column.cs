﻿using UnityEngine;
using System.Collections;

public class Column : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D collider){
	
		if(collider.GetComponent<Bird>() != null){
		
			if (!collider.GetComponent<Bird> ().isDead) {
				collider.GetComponent<Bird> ().Score++;
				GameControl.instance.UpdateScore ();
				AudioManager.instance.PlayPointSound ();
			}
		}
	}
}
