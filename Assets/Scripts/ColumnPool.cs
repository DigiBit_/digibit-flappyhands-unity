﻿using UnityEngine;
using System.Collections;

public class ColumnPool : MonoBehaviour 
{
	public static ColumnPool instance;
	public enum Level{Easy, Medium, Hard};
	public GameObject ColumnHard;
	public float ColumnHardMin;
	public float ColumnHardMax;
	public GameObject ColumnMedium;
	public float ColumnMediumMin;
	public float ColumnMediumMax;
	public GameObject ColumnEasy;
	public float ColumnEasyMin;
	public float ColumnEasyMax;


	public int columnPoolSize = 5;									//How many columns to keep on standby.
	public float spawnRate = 3f;									//How quickly columns spawn.


	private GameObject[] columns;									//Collection of pooled columns.
	private int currentColumn = 0;									//Index of the current column in the collection.

	private Vector2 objectPoolPosition = new Vector2 (-15,-25);		//A holding position for our unused columns offscreen.
	private float spawnXPosition = 10f;

	private float timeSinceLastSpawned;

	public Level level;


	GameObject columnPrefab;								//The column game object.
	float columnMin = -1f;									//Minimum y value of the column position.
	float columnMax = 3.5f;									//Maximum y value of the column position.

	public void Start()
	{
		instance = this;
		timeSinceLastSpawned = 0f;

		//Select obstacle based on level
		switch (level) {
			case Level.Easy:
				columnPrefab = ColumnEasy;
				columnMin = ColumnEasyMin;
				columnMax = ColumnEasyMax;
			break;

			case Level.Medium:
				columnPrefab = ColumnMedium;
				columnMin = ColumnMediumMin;
				columnMax = ColumnMediumMax;
			break;
			case Level.Hard:
				columnPrefab = ColumnHard;
				columnMin = ColumnHardMin;
				columnMax = ColumnHardMax;
			break;
		}

		//Initialize the columns collection.
		columns = new GameObject[columnPoolSize];
		//Loop through the collection... 
		for(int i = 0; i < columnPoolSize; i++)
		{
			//...and create the individual columns.
			columns[i] = (GameObject)Instantiate(columnPrefab, objectPoolPosition, Quaternion.identity);
		}
	}


	//This spawns columns as long as the game is not over.
	void Update()
	{
		if (GameControl.instance._2PlayerRaceMode)
			return;
		
		timeSinceLastSpawned += Time.deltaTime;

		if (!GameControl.instance.GameOver && timeSinceLastSpawned >= spawnRate && !GameControl.instance.GamePaused) 
		{	
			timeSinceLastSpawned = 0f;

			//Set a random y position for the column
			float spawnYPosition = Random.Range(columnMin, columnMax);

			//...then set the current column to that position.
			columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);

			//Increase the value of currentColumn. If the new size is too big, set it back to zero
			currentColumn ++;

			if (currentColumn >= columnPoolSize) 
			{
				currentColumn = 0;
			}
		}
	}
}