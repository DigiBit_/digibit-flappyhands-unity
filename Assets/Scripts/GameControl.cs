﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DigiBitSDK;

public class GameControl : MonoBehaviour 
{
	public static GameControl instance;			
	public Text scoreText;						
	public GameObject gameOvertext;
	public bool GameOver = false;	
	public float scrollSpeed = -1.5f;

	public bool GamePaused = true;
	public bool GameEnd = false;
	public bool GoToNextGame = false;
	public GameObject GamePausedtext;	
	public bool PlayWithoutDigibits = true;

	public GameObject BirdPrimary;
	public GameObject BirdSecondary;
	public GameObject MenuObject;
	public Text HighScoreText;
	public Text WhichBirdWonText;
	public Text TapToRestartText;

	public bool StartDelayStarted = false;
	public bool LevelSelected = false;
	bool ShowLevelSelection = true;

	public bool _1PlayerMode = true;
	public bool _2PlayerMode = false;
	public bool _2PlayerRaceMode = false;

	public GameObject ObstacleModeScene;
	public GameObject RaceModeScene;

	public Vector3 SecondaryBirdRacePosition;
	public Vector3 SecondaryBirdObstaclePosition;

	void Start()
	{
		instance = this;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.orientation = ScreenOrientation.Landscape;
		scoreText.enabled = false;
	}

	void Update()
	{
		CheckForGameRestart ();

        if (SceneManager.GetSceneByName("PluginStartUp").isLoaded)
        {
            PauseGame();
            return;
        }

        if ((!DigiBit.PrimaryConnected || !DigiBit.SecondaryConnected) && !PlayWithoutDigibits)
			PauseGame ();
		else
			UnpauseGame ();
	}
		
	#region GameRestart
	void CheckForGameRestart()
	{
		//If the game is over and the player has pressed some input...
		if (GameOver && (DigiBit.PrimaryData.Tap || DigiBit.SecondaryData.Tap || (PlayWithoutDigibits && Input.GetMouseButtonDown (0))))
			//Ensure TAP is gone before proceeding.
			GoToNextGame = true;
		else if (GoToNextGame && !DigiBit.PrimaryData.Tap && !DigiBit.SecondaryData.Tap)
			// Make sure Taps are gone.
			ResetScene ();
	}

	//Reset Scene to Main menu
	void ResetScene()
	{
		BirdPrimary.GetComponent<Bird> ().ResetBird ();
		BirdSecondary.GetComponent<Bird> ().ResetBird ();
		gameOvertext.SetActive (false);

		GamePaused = true;
		GameOver = false;
		GameEnd = false;
		GoToNextGame = false;
		ShowLevelSelection = true;
		HighScoreText.enabled = false;
		scoreText.enabled = false;
		AudioManager.instance.PlayBackgroundAudio ();
		ObstacleModeScene.SetActive (true);
		RaceModeScene.SetActive (false);
		BirdSecondary.transform.position = SecondaryBirdObstaclePosition;
	}

	#endregion

	#region GamePauseHandling

	void PauseGame()
	{
		if (!GameOver)
			GamePaused = true;

		//GamePausedtext.GetComponent<Text> ().text = "Turn On DigiBits\n(Hint, Power Button)\n Connecting...";
		//GamePausedtext.SetActive (true);

		BirdPrimary.SetActive (false);
		BirdSecondary.SetActive (false);
	}

	void UnpauseGame()
	{
		if (GamePaused && !StartDelayStarted) {
			//GamePausedtext.SetActive (false);
			StartCoroutine (GameStartDelay ());
			StartDelayStarted = true;
            DigiBitInputController.instance.SetLeds();
		} 
		//else if (!GamePaused)
			//GamePausedtext.SetActive (false);
	}

	IEnumerator GameStartDelay(){

		yield return StartCoroutine(CheckIfLevelSelectionNeeded ());
		SetupGameScene ();

		if (_2PlayerRaceMode)
			GamePausedtext.GetComponent<Text> ().text = "Reach the end of screen\n to win\nStart Flapping in 3";
		else
			GamePausedtext.GetComponent<Text> ().text = "Start Flapping in 3";
		GamePausedtext.SetActive (true);
		yield return new WaitForSeconds (1.0f);

		if (_2PlayerRaceMode)
			GamePausedtext.GetComponent<Text> ().text = "Reach the end of screen\n to win\nStart Flapping in 2";
		else
			GamePausedtext.GetComponent<Text> ().text = "Start Flapping in 2";
		yield return new WaitForSeconds (1.0f);

		if (_2PlayerRaceMode)
			GamePausedtext.GetComponent<Text> ().text = "Reach the end of screen\n to win\nStart Flapping in 1";
		else
			GamePausedtext.GetComponent<Text> ().text = "Start Flapping in 1";
		yield return new WaitForSeconds (1.0f);

		GamePausedtext.SetActive (false);
		GamePaused = false;
		StartDelayStarted = false;
	}

	IEnumerator CheckIfLevelSelectionNeeded()
	{
		//Displays MainMenu and waits for level selection if Game not started yet
		if (ShowLevelSelection) {

			scoreText.enabled = false;
			BirdPrimary.SetActive (false);
			BirdSecondary.SetActive (false);

			for(int i = 0; i < FindObjectsOfType<Column> ().Length; i++)
				Destroy (FindObjectsOfType<Column> ()[i].gameObject);
			yield return new WaitForSeconds (0.2f);

			MenuObject.SetActive (true);

			LevelSelected = false;
			while(!LevelSelected)
				yield return new WaitForSeconds (0.1f);

			MenuObject.SetActive (false);
			ShowLevelSelection = false;
			GetComponent<ColumnPool> ().Start ();
		}
	}

	void SetupGameScene()
	{		
		BirdPrimary.SetActive (true);
		scoreText.enabled = true;
		TapToRestartText.enabled = false;
		if(_2PlayerMode || _2PlayerRaceMode)
			BirdSecondary.SetActive (true);
		if (_2PlayerRaceMode) {
			ObstacleModeScene.SetActive (false);
			RaceModeScene.SetActive (true);
			BirdSecondary.transform.position = SecondaryBirdRacePosition;
			scoreText.enabled = false;
		}
		UpdateScore ();
	}
		
	#endregion

	#region LevelSelectionButtonCalls
	//called from MainMenu Easy button on click
	public void EasySelected(){
		GetComponent<ColumnPool> ().level = ColumnPool.Level.Easy;
	}

	//called from MainMenu Medium button on click
	public void MediumSelected(){
		GetComponent<ColumnPool> ().level = ColumnPool.Level.Medium;
	}

	//called from MainMenu Hard button on click
	public void HardSelected(){
		GetComponent<ColumnPool> ().level = ColumnPool.Level.Hard;
	}

	//called from MainMenu StartRace button on click
	public void StartRace(){
	}
	#endregion


	#region ScoreHandling

	//Updates score called from Column.cs
	public void UpdateScore()
	{
		if(_2PlayerMode)
			scoreText.text = "Yellow Score: " + BirdPrimary.GetComponent<Bird> ().Score + "\nGreen Score: " + BirdSecondary.GetComponent<Bird> ().Score;
		else
			scoreText.text = "Score: " + BirdPrimary.GetComponent<Bird> ().Score;
	}

	//Sets variable and text for birddied called from bird.cs
	public void BirdDied()
	{
		if ((!BirdPrimary.GetComponent<Bird> ().isDead ||
			!BirdSecondary.GetComponent<Bird> ().isDead) && !_1PlayerMode)
			return;
		
		if (_2PlayerRaceMode) {
			RaceFinished ();
			return;
		}
		WhichBirdWonText.color = Color.black;
		WhichBirdWonText.text = "";
		if(_2PlayerMode){
			if (BirdPrimary.GetComponent<Bird> ().Score > BirdSecondary.GetComponent<Bird> ().Score) {
				WhichBirdWonText.text = "Yellow Won !!!";
				WhichBirdWonText.color = Color.yellow;
			} else if (BirdPrimary.GetComponent<Bird> ().Score < BirdSecondary.GetComponent<Bird> ().Score) {
				WhichBirdWonText.text = "Green Won !!!";
				WhichBirdWonText.color = Color.green;
			}
			else
				WhichBirdWonText.text = "Score Tied !!!";
		}
		gameOvertext.SetActive (true);
		UpdateHighScore ();
		HighScoreText.enabled = true;
		GameEnd = true;
		StartCoroutine(EndOfGame ());
	}


	public void RaceFinished()
	{
		BirdPrimary.GetComponent<Bird> ().EnableDynamics = false;
		BirdSecondary.GetComponent<Bird> ().EnableDynamics = false;

		WhichBirdWonText.text = "";
		WhichBirdWonText.color = Color.black;
		if (BirdPrimary.GetComponent<Bird> ().WonRace && !BirdSecondary.GetComponent<Bird> ().WonRace) {
			WhichBirdWonText.text = "Yellow Won !!!";
			WhichBirdWonText.color = Color.yellow;
		} else if (!BirdPrimary.GetComponent<Bird> ().WonRace && BirdSecondary.GetComponent<Bird> ().WonRace) {
			WhichBirdWonText.text = "Green Won !!!";
			WhichBirdWonText.color = Color.green;
		}
		else if (BirdPrimary.GetComponent<Bird> ().WonRace && BirdSecondary.GetComponent<Bird> ().WonRace)
			WhichBirdWonText.text = "Race Tied !!!";
		else
			WhichBirdWonText.text = "Both Lost !!!";
		
		gameOvertext.SetActive (true);
		GameEnd = true;
		StartCoroutine(EndOfGame ());
	}

	void UpdateHighScore(){

		int HighScore = PlayerPrefs.GetInt ("HighScore");

		if (BirdPrimary.GetComponent<Bird> ().Score > HighScore) {
			HighScore = BirdPrimary.GetComponent<Bird> ().Score;
			PlayerPrefs.SetInt ("HighScore", HighScore);
		}

		if (BirdSecondary.GetComponent<Bird> ().Score > HighScore) {
			HighScore = BirdSecondary.GetComponent<Bird> ().Score;
			PlayerPrefs.SetInt ("HighScore", HighScore);
		}

		HighScoreText.text = "High Score: " + HighScore;

	}

	#endregion

	IEnumerator EndOfGame()
	{
		yield return new WaitForSeconds (3.0f);
		TapToRestartText.enabled = true;
		GameOver = true;
		GoToNextGame = false;
	}

}