﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public AudioSource PointSound;
	public AudioSource HitSound;
	public AudioSource BackgroundAudio;
	public AudioSource FlappingSound;

	public static AudioManager instance;
	void Start () {
		instance = this;
		BackgroundAudio.Play ();
	}

	public void PlayPointSound(){
		PointSound.Play ();
	}

	public void PlayHitSound(){
		BackgroundAudio.Stop ();
		HitSound.Play ();
	}

	public void PlayFlappingSound(){
		FlappingSound.Play ();
	}

	public void PlayBackgroundAudio(){
		BackgroundAudio.Play ();
	}
}
